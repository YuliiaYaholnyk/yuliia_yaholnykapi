import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../dto/image_dto.dart';

class ImageDetailScreen extends StatefulWidget {
  static const routeName = '/imageDetail';

  @override
  _ImageDetailScreenState createState() => _ImageDetailScreenState();
}

class _ImageDetailScreenState extends State<ImageDetailScreen> {
  //_ImageDetailScreenState();
  //final int imageIndex;
  bool _selected = false;

  @override
  Widget build(BuildContext context) {
    ImageDto data = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 300,
            pinned: true,
            title: Text('${data.altDescription}'),
            flexibleSpace: FlexibleSpaceBar(
              background: Hero(
                tag: '${data.id}',
                child: Image.network(
                  data.imageUrls.regular,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
//              Container(
//                  color: Colors.blueGrey,
//                  padding: EdgeInsets.symmetric(horizontal: 10),
//                  width: double.infinity,
//                  height: 100,
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text(
//                        '${data.altDescription}',
//                        textAlign: TextAlign.center,
//                        softWrap: true,
//                        style: TextStyle(fontSize: 20, color: Colors.white),
//                      ),
//                    ],
//                  )),

              SizedBox(
                height: 700,
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
