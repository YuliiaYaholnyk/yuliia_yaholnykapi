import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:workWithApi/helpers/http_connection.dart';
import 'package:workWithApi/providers/connection.dart';
import 'package:workWithApi/widgets/build_money_screen.dart';

import 'package:workWithApi/widgets/future_screen.dart';
import 'package:workWithApi/screens/second_page_screen.dart';

import 'no_internet.dart';

class MonetScreen extends StatelessWidget {
  static const routeName = '/monetScreen';

  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => SecondPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          title: Text('Exchange Rates'),
          actions: <Widget>[
            FlatButton(
              child: Text('Next page'),
              onPressed: () {
                Navigator.of(context).pushReplacement(_createRoute());
              },
            )
          ],
        ),
        body: BuildMoneyScreen() ,

    );
  }
}




