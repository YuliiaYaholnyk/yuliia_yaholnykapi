import 'package:flutter/material.dart';

class NoInternet extends StatelessWidget {
  static const routeName = '/no-internet';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('No Internet'),
        ),
        body: Center(
          child: Text(
            'You have no internet',
            textAlign: TextAlign.center,
          ),
        ));
  }
}
