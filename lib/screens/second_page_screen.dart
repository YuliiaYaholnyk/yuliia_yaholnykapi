import 'package:flutter/material.dart';
import 'package:workWithApi/dto/image_dto.dart';
import '../repositories/image_repository.dart';
import '../screens/image_detail_screen.dart';

class SecondPage extends StatelessWidget {
  static const routeName = '/secondPage';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
      body: FutureBuilder(
          future: ImageRepository.instance.getMoneyDto(),
          builder: (context, snapshot) {
            final data = snapshot.data as List<ImageDto>;
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return GridView.builder(
                padding: const EdgeInsets.all(10.0),
                itemCount: data.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                  childAspectRatio: 3 / 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
                itemBuilder: (ctx, i) => ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: GridTile(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushNamed(
                            ImageDetailScreen.routeName,
                            arguments: data[i]);
                      },
                      child: Hero(
                        tag: '${data[i].id}',
                        child: FadeInImage(
                          placeholder: AssetImage('assets/dart-logo.png'),
                          image: NetworkImage(data[i].imageUrls.regular),
                          fit: BoxFit.cover,
                          width: double.infinity,
                        ),
                      ),
                    ),
                    footer: GridTileBar(
                      backgroundColor: Colors.black87,
                      title: data[i].altDescription == null
                          ? Text('')
                          : Text(
                              data[i].altDescription,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12.0),
                              maxLines: 3,
                            ),
                    ),
                  ),
                ),
              );
            }
          }),
    );
  }
}
