import 'package:flutter/material.dart';
import 'dart:convert';
import '../dto/image_url_dto.dart';
class ImageDto {
  final String id;
  final String createdAt;
  final String description;
  final String altDescription;
  final ImageUrlsDto imageUrls;

  ImageDto({
    @required this.id,
    @required this.createdAt,
    @required this.description,
    @required this.altDescription,
    @required this.imageUrls,
  });

  factory ImageDto.fromJson(Map<String, dynamic> json) {
    return ImageDto(
      id: json["id"],
      createdAt: json["created_at"],
      description: json["description"],
      altDescription: json["alt_description"],
      imageUrls: ImageUrlsDto.fromJson(json["urls"]),
    );
  }
}
