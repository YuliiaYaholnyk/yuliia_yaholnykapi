import 'package:flutter/foundation.dart';

class MoneyDto {
  final String ccy;
  final String base_ccy;
  final String buy;
  final String sale;

  MoneyDto({
    @required this.ccy,
    @required this.base_ccy,
    @required this.buy,
    @required this.sale,
  });

  factory MoneyDto.fromJson(Map<String, dynamic> json) {
    return MoneyDto(
      ccy: json["ccy"],
      base_ccy: json["base_ccy"],
      buy: json["buy"],
      sale: json["sale"],
    );
  }
//

//

}