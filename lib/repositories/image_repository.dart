import '../services/image_service.dart';
import '../dto/image_dto.dart';

class ImageRepository {
  ImageRepository._privateConstructor();

  static final ImageRepository _instance = ImageRepository._privateConstructor();

  static ImageRepository get instance => _instance;

  Future<List<ImageDto>> getMoneyDto() async {
    return await ImageService.instance.getData();
  }
}
