import '../services/monet_service.dart';
import '../dto/money_dto.dart';

class MyRepository {
  MyRepository._privateConstructor();

  static final MyRepository _instance = MyRepository._privateConstructor();

  static MyRepository get instance => _instance;

  Future<List<MoneyDto>> getMoneyDto() async {
    return await MoneyService.instance.getData();
  }
}
