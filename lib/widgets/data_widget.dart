import 'package:flutter/material.dart';
import 'package:workWithApi/dto/money_dto.dart';

class DataWidget extends StatelessWidget {
  const DataWidget({
    Key key,
    @required this.dat,
  }) : super(key: key);

  final List<MoneyDto> dat;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dat.length,
      itemBuilder: (BuildContext ctx, int i) => Card(
        color: Colors.black45,
        elevation: 20.0,
        child: Container(
          height: 60.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    "BUY:",
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    dat[i].buy,
                    style: TextStyle(color: Colors.white),
                  )
                ],
              ),
              CircleAvatar(
                backgroundColor: Colors.green,
                radius: 20.0,
                child: Text(
                  '${dat[i].ccy}' + ' TO ' + '${dat[i].base_ccy}',
                  style: TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text("SALE:", style: TextStyle(color: Colors.white)),
                  Text(dat[i].sale, style: TextStyle(color: Colors.white)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}