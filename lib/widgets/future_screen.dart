import 'package:flutter/material.dart';
import 'package:workWithApi/helpers/http_connection.dart';
import 'package:workWithApi/screens/no_internet.dart';
import 'package:workWithApi/widgets/build_money_screen.dart';

class FutureWidget extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
      future: HttpConnection.checkInternetConnection(),
      builder: (BuildContext context, AsyncSnapshot snapshot1) {

        if (!snapshot1.data) {
          Navigator.of(context).pushReplacementNamed(NoInternet.routeName);
        }

        return BuildMoneyScreen();
      },
    );
  }
}