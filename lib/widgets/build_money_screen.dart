import 'package:flutter/material.dart';
import 'package:workWithApi/dto/money_dto.dart';
import 'package:workWithApi/helpers/http_connection.dart';
import 'package:workWithApi/repositories/my_repository.dart';
import 'package:workWithApi/widgets/data_widget.dart';
import '../screens/no_internet.dart';
class BuildMoneyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: MyRepository.instance.getMoneyDto(),
      builder: (BuildContext context, AsyncSnapshot snapshot2) {
        final dat = snapshot2.data as List<MoneyDto>;

        print(dat);

        if (snapshot2.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        }


          return DataWidget(dat: dat);

      },
    );
  }
}
