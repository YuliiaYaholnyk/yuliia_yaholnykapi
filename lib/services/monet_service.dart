import 'package:http/http.dart' as http;
import 'package:workWithApi/helpers/http_connection.dart';
import 'dart:convert';
import '../dto/money_dto.dart';
import '../helpers/http_helper.dart';
import '../const.dart';

class MoneyService {
  MoneyService._privateConstructor();

  static final MoneyService _instance = MoneyService._privateConstructor();

  static MoneyService get instance => _instance;

  Future<List<MoneyDto>> getData() async {
    final List<MoneyDto> data = [];
    await HttpConnection.checkInternetConnection();
    if (HttpConnection.isConnection) {
      try {
        List<dynamic> response =
        await HttpHelper.instance.request(MONEY_URL, HttpType.get);

        for (var item in response) {
          data.add(MoneyDto.fromJson(item));
        }

        return data;
      } catch (error) {
        print(error);
      }
    }else{
      return data;
    }
  }
}
