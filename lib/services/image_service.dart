import 'dart:convert';
import '../const.dart';
import 'package:http/http.dart' as http;
import '../dto/image_dto.dart';
import '../helpers/http_helper.dart';

class ImageService {

  ImageService._privateConstructor();

  static final ImageService _instance = ImageService._privateConstructor();

  static ImageService get instance => _instance;

  Future<List<ImageDto>> getData() async {
    try {
      List<dynamic> response =
      await HttpHelper.instance.request(IMAGE_URL, HttpType.get);



      final List<ImageDto> data = [];
      print('------------------------------------');
      for (var item in response) {
        data.add(ImageDto.fromJson(item));
      }

      return data;
    } catch (error) {
      print(error);
    }
  }
}
