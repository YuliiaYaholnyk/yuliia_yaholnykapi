import 'package:flutter/foundation.dart';
import 'dart:io';

class HttpConnection{
static bool isConnection = false;
 static Future<void> checkInternetConnection () async{

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
       HttpConnection.isConnection = true;
       return;
      }
    } on SocketException catch (_) {
      print('not connected');
    }
    HttpConnection.isConnection = false;

  }

  static Future<void> checkInternetLoop()async{
   await checkInternetConnection ();
   Future.delayed(Duration(seconds: 1)).then((value) {
     checkInternetLoop();
   });

  }




}