import 'dart:convert';

import 'package:http/http.dart' as http;
import '../helpers/http_connection.dart';

enum HttpType {
  get,
  put,
  post,
  delete,
}

class HttpHelper {
  HttpHelper._privateConstructor();

  static final HttpHelper _instance = HttpHelper._privateConstructor();

  static HttpHelper get instance => _instance;

  Future<List<dynamic>> request(String fullUrl, HttpType type) async {
    await HttpConnection.checkInternetConnection();
    if (!HttpConnection.isConnection) {
      return null;
    }

    http.Response response;
    if (type == HttpType.get) {
      response = await http.get(fullUrl);
    }
    if (response != null && json.decode(response.body) != null) {
      return json.decode(response.body) as List<dynamic>;
    }
  }
}
