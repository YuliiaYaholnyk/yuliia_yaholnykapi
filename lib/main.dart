import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './screens/money_screen.dart';
import './screens/second_page_screen.dart';
import './screens/image_detail_screen.dart';
import './providers/connection.dart';
import './helpers/custom_route.dart';
import './screens/no_internet.dart';
import './helpers/http_connection.dart';
import './providers/connection.dart';
import 'screens/second_page_screen.dart';

void main() {
  //HttpConnection.checkInternetConnection().then((_) => null);
//  runApp(
//    ChangeNotifierProvider(
//      builder: (context) => ConnectionProvider(),
//      child: MyApp(),
//    ),
//  );
//HttpConnection.checkInternetLoop().then((_) => null);
runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
//  void initState() {
//    final providerData =
//        Provider.of<ConnectionProvider>(context, listen: false);
//    providerData.connectionCheck();
//
//    super.initState();
//  }

  @override
  Widget build(BuildContext context) {
   // final providerData = Provider.of<ConnectionProvider>(context, listen: false);
    // providerData.connectionCheck();
   // print('build    ' + providerData.isConnection.toString());
  return   MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: Colors.grey,
            pageTransitionsTheme: PageTransitionsTheme(builders: {
              TargetPlatform.android: CustomPageTransitionBuilder(),
            })),
        initialRoute: SecondPage.routeName,
        routes: {
          MonetScreen.routeName: (BuildContext ctx) => MonetScreen(),
          SecondPage.routeName: (BuildContext ctx) => SecondPage(),
          NoInternet.routeName: (BuildContext ctx) => NoInternet(),
          ImageDetailScreen.routeName: (BuildContext ctx) =>
              ImageDetailScreen(),
        },

    );
  }
}
