import 'package:flutter/foundation.dart';
import 'package:workWithApi/helpers/http_connection.dart';
import '../dto/image_dto.dart';

class ConnectionProvider with ChangeNotifier {
  bool isConnection = false;

  void isConnected() {
    HttpConnection.checkInternetConnection().then(
      (_) {
        if (HttpConnection.isConnection == true) {
          isConnection = true;
        } else {
          isConnection = false;
        }
        notifyListeners();
      },
    );
  }

  void connectionCheck() {
    Future.delayed(Duration(seconds: 1)).then((value) {
      isConnected();
      print('isConnection :  ' + isConnection.toString());
    });
  }
}
